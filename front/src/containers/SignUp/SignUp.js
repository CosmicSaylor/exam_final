import React, {useRef, useState} from 'react';
import {useSelector, useDispatch} from "react-redux";
import {Link} from "react-router-dom";
import {registerUser} from "../../store/actions/usersActions";
import './SignUp.css';

const SignUp = () => {
    const [state, setState] = useState({
        email: '',
        password: '',
        displayName: '',
        avatarImage: ''
    });
    const inputRef = useRef();

    const dispatch = useDispatch();
    const error = useSelector(state => state.users.registerError);

    const inputChangeHandler = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };
    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setState(prevState => ({
            ...prevState, [name]: file
        }));
    };
    const formSubmit = (e) => {
        e.preventDefault();
        const formData = new FormData();
        Object.keys(state).forEach(key => {
            formData.append(key, state[key]);
        });
        dispatch(registerUser(formData));
    };


    return (
        <div className='signUpPageInner'>
            <div className='signUpBlock'>
                <h3>Sign Up</h3>
                <input
                    type='text'
                    name='email'
                    className='signUpField'
                    value={state.email}
                    onChange={inputChangeHandler}
                    placeholder='Email'
                />
                <input
                    type='text'
                    name="displayName"
                    className='signUpField'
                    value={state.displayName}
                    onChange={inputChangeHandler}
                    placeholder='Display Name'
                />
                <input
                    className='signUpField'
                    name="password"
                    type="password"
                    value={state.password}
                    onChange={inputChangeHandler}
                    placeholder='Password'
                />
                <button
                    type='button'
                    className='signBtn'
                    onClick={formSubmit}
                >
                    Sign In
                </button>
                <Link to="/signin">
                    Already have an account? Sign In
                </Link>
            </div>
        </div>

    );
}

export default SignUp;